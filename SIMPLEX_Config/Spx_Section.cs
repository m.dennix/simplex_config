﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMPLEX_Config
{
    /// <summary>
    /// Rappresenta una sezione di un file di configurazione.
    /// <pre></pre>
    /// ----
    /// @MdN 
    /// Crtd: 28/11/2013 - v. 1.0.0.0
    /// Mdfd: 06/01/2016 - v. 1.0.0.2 - corretto BUG sulla property length
    /// Mdfd: 14/01/2016 - Monterotondo
    /// Mdfd: 26/03/016 23:09 - Livorno
    /// </summary>
    public class Spx_Section
    {
        /// <summary>
        /// Istanziatore.
        /// @MdN 28/11/2013
        /// </summary>
        /// <param name="p_Name">Nome dell'istanza.</param>
        /// <returns>la nuova istanza.</returns>
        public static Spx_Section createSection(String p_Name)
        {
            if (p_Name == null)
            {
                Spx_Message E = new Spx_Message();
                E.MessageCode = 4;
                E.Message = "Spx_Section.createSection(): invalid name";
                throw E;
            }
            return new Spx_Section(p_Name);
        }

        /// <summary>
        /// Crea una nuova sezione e la chiama come il parametro.
        /// @MdN 28/11/2013
        /// </summary>
        /// <param name="p_Name">Nome della sezione e stringa identificativa della medesima.</param>
        private Spx_Section(String p_Name)
        {
            Name = p_Name;
            head = null;
        }

        
        private Spx_ConfigEntry head;
        private String Name;

        #region METODI PUBBLICI
        /// <summary>
        /// Rimuove, se esiste, la chiave specificata dalla sezione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_Key">Nome della chiave da rimuovere</param>
        /// <returns>True se la chiave viene trovata ed eliminata, false altrimenti.</returns>
        public Boolean remove(String p_Key)
        {
            Spx_ConfigEntry _temp = null;
            Spx_ConfigEntry _prev = null;
            
            // * Controllo
            if (p_Key == null)
                return false;

            // Ciclo di ricerca della chiave
            _temp = head;
            while (_temp != null)
            {
                if (_temp.Key.CompareTo(p_Key) == 0)
                {
                    // TROVATA!!!
                    if (_temp.Key == head.Key)
                    {
                        // rimozione
                        head = _temp.next;
                        _temp.next = null;
                        return true;
                    }
                    else
                    {
                        // rimozione
                        _prev.next = _temp.next;
                        _temp.next = null;
                        return true;
                    }
                    _prev = _temp;
                    _temp = _temp.next;
                }                
            }
            return false;
        }//fine metodo

        /// <summary>
        /// Cerca nella sezione il valore della chiave specificata.
        /// @MdN 28/11/2013
        /// </summary>
        /// <param name="P_Key">Chiave di ricerca</param>
        /// <returns>
        /// Una stringa che rappresenta il valore della chiave cercata.
        /// </returns>
        /// <exception cref="Spx_Message">Lancia un'eccezione di tipo Spx_Message così codificata:
        /// 1.Il parametro chiave contiene NULL
        /// 2.Sezione vuota.
        /// 3.Chiave non trovata.
        /// </exception>
        public String searchFor(String p_Key)
        {
            Spx_Message E = new Spx_Message();
            Spx_ConfigEntry tmp;

            if (p_Key == null)
            {
                E.MessageCode = 1;
                E.Message = "Spx_Section.searchFor(): key not specified.";
                throw E;
            }

            if (head == null)
            {
                E.MessageCode = 2;
                E.Message = "Spx_Section.searchFor(): Empty Section.";
                throw E;
            }
            // RICERCA
            tmp = head;
            while (tmp!=null)
            {
                if (tmp.Key.CompareTo(p_Key) == 0)
                    return tmp.Value;
                else
                    tmp = tmp.next;
            }
            E.Message = "Spx_Section.searchFor(): Key " + p_Key + " not found."; //@MdN 26/03/016 23:09 - Livorno
            E.MessageCode = 3;
            throw E;
        }

        /// <summary>
        /// Aggiunge una nuova voce alla sezione ma non inserisce chiavi duplicate.
        /// Se la chiave specificata già esiste, non viene compiuta alcuna operazione.
        /// <see cref="SIMPLEX_Config.addWithReplace"/>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 23/01/2016 - Monterotondo - add() non aggiunge una chiave duplicata
        /// </pre>
        /// </summary>
        /// <param name="p_Key">Chiave</param>
        /// <param name="p_Value">Valore della chiave</param>
        public void add(String p_Key, String p_Value)
        {
            Spx_ConfigEntry tmp = new Spx_ConfigEntry();
            Spx_ConfigEntry Current = null;
            Spx_ConfigEntry Prev = null;

            tmp.Key = p_Key;
            tmp.Value = p_Value;
            if (head == null)
            {
                head = tmp;
                return;
            }
            // AGGIUNGE IN MODO ORDINATO
            Current = head;
            while (Current!=null)
            {
                if (Current.Key.CompareTo(p_Key) < 0)
                {
                    Prev = Current;
                    Current = Current.next;
                    //scorre in avanti
                }else
                {
                    //<added @MdN: 23/01/2016>
                    if (Current.Key.CompareTo(p_Key) == 0)
                    {
                        //salta ma lancia un messaggio
                        Spx_Message _spxm = new Spx_Message();
                        _spxm.Message = "The key " + p_Key +  " already exists";
                        _spxm.MessageCode = 9;
                        _spxm.Source = "Spx_Section.add()";
                        throw _spxm;                        
                    }
                    //<added @MdN: 23/01/2016>
                    //Inserimento
                    tmp.next = Current;
                    if (Prev == null)
                    {   //inserimento in testa
                        head = tmp;
                    }
                    else
                    {
                        Prev.next = tmp;
                    }
                    return;
                }
            }
            //se il ciclo arriva alla fine --> si aggiunge in coda!
            Prev.next = tmp;
            return;
        }

        ///<summary>
        /// Aggiunge una nuova voce alla sezione ma non inserisce chiavi duplicate.
        /// Se la chiave specificata già esiste, il vacchio valore viene sostituito 
        /// da quello passato come parametro (p_Value).
        /// 
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 23/01/2016
        /// </pre>
        /// </summary>
        /// <param name="p_Key"></param>
        /// <param name="p_Value"></param>
        public void addWithReplace(String p_Key, String p_Value)
        {
            Spx_ConfigEntry tmp = new Spx_ConfigEntry();
            Spx_ConfigEntry Current = null;
            Spx_ConfigEntry Prev = null;

            tmp.Key = p_Key;
            tmp.Value = p_Value;
            if (head == null)
            {
                head = tmp;
                return;
            }
            // AGGIUNGE IN MODO ORDINATO
            Current = head;
            while (Current != null)
            {
                if (Current.Key.CompareTo(p_Key) < 0)
                {
                    Prev = Current;
                    Current = Current.next;
                    //scorre in avanti
                }
                else
                {                    
                    if (Current.Key.CompareTo(p_Key) == 0)
                    {
                        Current.Value = p_Value;
                        return;
                    }
                    //Inserimento
                    tmp.next = Current;
                    if (Prev == null)
                    {   //inserimento in testa
                        head = tmp;
                    }
                    else
                    {
                        Prev.next = tmp;
                    }
                    return;
                }
            }
            //se il ciclo arriva alla fine --> si aggiunge in coda!
            Prev.next = tmp;
            return;
        }


        #endregion


        /// <summary>
        /// REstituisce il nome della sezione.
        /// @MdN - 28/11/213
        /// </summary>
        public String name
        {
            get { return Name; }
        }

        /// <summary>
        /// Compone una stringa con il contenuto della sezione in termini di chiave-valore.
        /// @MdN - 05/12/2013.
        /// </summary>
        /// <returns>Stringa che rappresenta la sezione.</returns>
        public String toString()
        {
            Spx_ConfigEntry l_temp = null;
            StringBuilder toReturn = new StringBuilder();
            // Intestazione
            toReturn.Append("Section [").Append(Name).Append("]\n");
            l_temp = this.head;
            while (l_temp!=null)
            {
                toReturn.Append(l_temp.Key).Append("=").Append(l_temp.Value).Append("\n");
                l_temp = l_temp.next;
            }
            return toReturn.ToString();
        }
        /// <summary>
        /// Restituisce l'elenco delle chiavi della sezione in una matrice di stringhe.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 26/12/2015 - Livorno - v. 1.0.0.1
        /// </pre>
        /// </summary>
        public String[] Keys
        {
            get
            {
                Int16 _len = length;
                Spx_ConfigEntry _tmp = null;
                Int16 _cnt = 0;

                if (_len == 0)
                    return null;
                _tmp = head;
                if (head == null)
                    return null;
                String[] _toret = new String[_len];
                // ciclo
                while (_tmp != null)
                {
                    _toret[_cnt] = _tmp.Key;
                    _tmp = _tmp.next;
                    _cnt++;
                }
                return _toret;
            }
        }

        /// <summary>
        /// Restituisce il numero di chiavi costituenti la sezione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 26/12/2015
        /// Mdfd: 06/01/2016 v. 1.0.0.2 - corretto BUG
        /// </pre>
        /// </summary>
        public Int16 length
        {
            get
            {
                Spx_ConfigEntry _tmp = null;
                Int16 _cnt = 0;
                _tmp = head;

                while (_tmp != null)
                {
                    _tmp = _tmp.next;
                    _cnt++;
                }

                //return (Int16)(_cnt + 1); // BUG. <removed @MdN: 06/01/2016>
                return (Int16)(_cnt);       // BUG. <added @MdN: 06/01/2016>
            }
        }
    }
}
