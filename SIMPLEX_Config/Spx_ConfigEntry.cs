﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMPLEX_Config
{
    /// <summary>
    /// Entry costituita da una coppia Chiave (Key)- Valore (Value).
    /// </summary>
    class Spx_ConfigEntry
    {
        public String Key;
        public String Value;
        public String EntryType;
        public Spx_ConfigEntry next;

    }
}
