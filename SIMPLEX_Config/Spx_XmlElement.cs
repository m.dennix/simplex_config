﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMPLEX_Config
{
    /// <summary>
    /// <p>
    /// Nodo che rappresenta un Elemento del file XML. Il contenuto di un file XML può essere un testo
    /// o uno o più elementi di livello inferiore.
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: ????
    ///  **** v. 1.0.1.0
    /// Mdfd: 17/08/2017 - Ledro        - v. 1.0.1.0: introdotta appendElementAfter()
    /// Mdfd: 22/08/2017 - Monterotondo - v. 1.0.1.0: introdotta setAttribute(); introdotto getEntry()
    /// 
    /// </pre>
    /// </p>
    /// </summary>
    public class Spx_XmlElement
    {
        /// <summary>
        /// Elemento che si trova allo stesso livello: è il successivo nella sequenza
        /// </summary>
        internal Spx_XmlElement MyNext;
        /// <summary>
        /// Elemento che si trova al livello inferiore: è il primo di una eventuale sequenza
        /// </summary>
        internal Spx_XmlElement MyInner;
        /// <summary>
        /// Elemento che si trova al livello superiore: tutti gli elementi del medesimo livello
        /// hanno il medesimo elemento Father
        /// </summary>
        internal Spx_XmlElement MyFather;
        /// <summary>
        /// Primo elemento della eventuale sequenza di attributi
        /// </summary>
        internal Spx_ConfigEntry MyHead;
        /// <summary>
        /// <p>
        /// Il primo di una sequenza di elementi che stanno allo stesso livello
        /// rispetto ad uno stesso elemento padre.
        /// </p>
        /// </summary>
        internal Spx_XmlElement MyChief;
        /// <summary>
        /// Testo.
        /// </summary>
        internal String MyText;
        /// <summary>
        /// Valore intero che rappresenta il tipo di nodo XML
        /// <see cref="XmlNodeType"/>
        /// </summary>
        internal Int32 MyType;
        /// <summary>
        /// Questo attributo DEVE essere true solo in fase di costruzione della struttura.
        /// 
        /// </summary>
        internal Boolean _opened = false;
        /// <summary>
        /// <p>
        /// Nome dell'attributo
        /// </p>
        /// </summary>
        internal String MyName;

        #region COSTRUTTORI
        /// <summary>
        /// Costruttore futile (dummy). Serve per restringere l'accesso.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/05/2014
        /// </pre>
        /// 
        /// </summary>
        internal Spx_XmlElement()
        {
            //DUMMY CONSTRUCTOR
        }
        #endregion

        #region STATIC METHODS

        /// <summary>
        /// <p>
        /// Crea l'elemento radice (root) della struttura ad albero che rappresenta
        /// una configurazione con il nome specificato in argomento.
        /// </p>
        /// <p>
        /// Se il nome non viene specificato (cioè il parametro è null), l'elemento 
        /// prende il nome di default 'root'.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_name">Nome dell'elemento radice</param>
        /// <returns>L'elemento creato.</returns>
        public static Spx_XmlElement createRoot(String p_name)
        {
            Spx_XmlElement _toRet = new Spx_XmlElement();

            if (p_name != null)
                _toRet.MyName = p_name;
            else
                _toRet.MyName = "root";

            return _toRet;
        }

        #endregion

        #region PROPERTIES

        /// <summary>
        /// <p>
        /// Restituisce il numero di elementi di livello inferiore all'elemento corrente.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 04/04/2015
        /// </pre>
        /// </summary>
        public int InnerElementsCount
        {
            get
            {
                int t = 0;
                Spx_XmlElement _current = MyInner;
                while (_current != null)
                {
                    _current = _current.MyInner;
                    t++;
                }
                return t;
            }
        }
        
        
        /// <summary>
        /// <p>
        /// Restituisce il numero di attributi di un elemento
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 02/04/2015
        /// </pre>
        /// </summary>
        public int AttributesCount
        {
            get
            {
                Spx_ConfigEntry _head = MyHead;
                int _toRet=0;
                while (_head != null)
                {
                    _head = _head.next;
                    _toRet++;
                }
                return _toRet;
            }
        }

        /// <summary>
        /// <p>
        /// Restituisce l'elemento di livello superiore/esterno.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 02/04/2015
        /// </pre>
        /// </summary>
        public Spx_XmlElement Father
        {
            get
            {
                return MyFather;
            }
        }
        /// <summary>
        /// <p>
        /// Restituisce l'elemento successivo nel medesimo livello.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 02/04/2015
        /// </pre>
        /// </summary>
        public Spx_XmlElement Next
        {
            get
            {
                return MyNext;
            }
        }
        /// <summary>
        /// <p>
        /// Restituisce il primo elemento di livello più inferiore/interno.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 02/04/2015
        /// </pre>
        /// </summary>
        public Spx_XmlElement Inner
        {
            get
            {
                return MyInner;
            }
        }

        /// <summary>
        /// <p>
        /// Restituisce il primo degli elementi di egual livello 
        /// (il capo della lista a cui appartiene l'elemento corrente).
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 02/04/2015
        /// </pre>
        /// </summary>
        public Spx_XmlElement Chief
        {
            get
            {
                return MyChief;
            }
        }

        /// <summary>
        /// <p>
        /// Restituisce il testo dell'elemento corrente.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 02/04/2015
        /// </pre>
        /// </summary>
        public String Text
        {
            get
            {
                return MyText;
            }
        }

        /// <summary>
        /// <p>
        /// Restituisce il nome dell'elemento corrente.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 02/04/2015
        /// </pre>
        /// </summary>
        public String Name
        {
            get
            {
                return MyName;
            }
        }
        #endregion

        #region PRIVATE, PROTECTED AND INTERNAL METHODS

        /// <summary>
        /// Restituisce un riferimento all'oggetto Entry che rappresenta un attributo.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 22/08/2017 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_AttributeName">Nome dell'attributo di cui si vuol ottenere il corrispondente oggetto Entry.</param>
        /// <returns>L'oggetto Entry corrispondente all'attributo richiesto.</returns>
        private Spx_ConfigEntry getEntry(String p_AttributeName)
        {
            Spx_ConfigEntry _current = MyHead;
            while(_current != null)
            {
                if (_current.Key.CompareTo(p_AttributeName) == 0)                
                    return _current;
                _current = _current.next;
            }
            return null;
        }

        /// <summary>
        /// <p>
        /// Restituisce l'attributo in posizione p_i (base 0)
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/04/2015
        /// </pre>
        /// </summary>
        /// <param name="p_i">indice base 0</param>
        /// <returns>l'atributo in posizione p_i ovvero null.</returns>
        private Spx_ConfigEntry getEntryAt(int p_i)
        {
            if (p_i >= AttributesCount)
                return null;
            if (MyHead == null)
                return null;
            int t = 0;
            Spx_ConfigEntry _temp = MyHead;
            while (t < p_i)
            {
                _temp = _temp.next;
                t++;
            }
            return _temp;
        }

        /// <summary>
        /// <p>
        /// Aggiunge un attributo all'elemento.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/04/2015
        /// </pre>
        /// </summary>
        /// <param name="p_name">Nome dell'attributo</param>
        /// <param name="p_value">Valore dell'attributo</param>
        /// <returns>Numero totale di attributi compreso quello aggiunto.</returns>
        internal int Add(String p_name, String p_value)
        {
            Spx_ConfigEntry _Entry = new Spx_ConfigEntry();
            Spx_ConfigEntry _head = MyHead;
            int _toRet = 0;
            _Entry.Key = p_name;
            _Entry.Value = p_value;
            if (MyHead == null)
            {
                MyHead = _Entry;
                return 1;
            }
            while (_head.next != null)
            {
                _head = _head.next;
                _toRet++;
            }
            _head.next = _Entry;
            return _toRet + 1;
        }

        #endregion


        #region PUBLIC  METHODS
        /// <summary>
        /// Imposta il valore di un attributo, se presente, altrimenti esce e restituisce -1.<br></br>
        /// Usato per modificare il valore di un attributo.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 22/08/2017 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_Name">Nome dell'attributo</param>
        /// <param name="p_Value">Valore dell'attributo</param>
        public void setAttribute(String p_Name, String p_Value)
        {
            Spx_ConfigEntry _toModify = getEntry(p_Name);
            if (_toModify != null)
                _toModify.Value = p_Value;
        }



        /// <summary>
        /// <p>
        /// Aggiunge un attributo all'elemento.
        /// </p>
        /// <p>
        /// E' un lifting di visibilità del metodo internal int Add(String, String).
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_name">Nome dell'attributo</param>
        /// <param name="p_value">Valore dell'attributo</param>
        /// <returns>Numero totale di attributi compreso quello aggiunto.</returns>
        public int addAttribute(String p_name, String p_value)
        {
            return Add(p_name, p_value);
        }

        /// <summary>
        /// <p>
        /// Crea un nuovo elemento e lo accoda agli elementi di pari livello
        /// dell'elemento corrente, eseguendo correttamente il binding.
        /// </p>
        /// <p>
        /// Se il parametro p_Name non viene specificato, il metodo esce senza creare
        /// e retituire alcun elemento.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_Name">nome dell'elemento (not null)</param>
        /// <param name="p_Text">corpo dell'elemento (o null)</param>
        /// <returns>L'elemento creato ed accodato.</returns>
        public Spx_XmlElement appendElement(String p_Name, String p_Text)
        {
            Spx_XmlElement _current = this;

            if (p_Name == null)
                return null;
            Spx_XmlElement _toRet = new Spx_XmlElement();
            _toRet.MyName = p_Name;
            _toRet.MyText = p_Text;
            // ACCODAMENTO
            while (_current.MyNext != null)
                _current = _current.MyNext;
            _current.MyNext = _toRet;
            // BIND            
            _toRet.MyChief= MyChief;
            _toRet.MyFather = MyFather;
            return _toRet;
        }
        
        /// <summary>
        /// Crea un nuovo elemento e lo inserisce dietro l'elemento corrente dopo un certo numero di posizioni,
        /// eseguendo correttamente il binding.
        /// <p>
        /// Se il numero di posizioni è 0 (p_index=0) l'elemente viene inserito subito dopo l'elemento corrente.<br></br> 
        /// Se il numero di posizioni eccede il numero di elementi che seguono quello corrente, l'effetto è equivalente 
        /// a quello ottenuto con il metodo appendElement(String p_Name, String p_Text), cioè un semplice accodamento.
        /// </p>
        /// <p>
        /// Se il parametro p_Name non viene specificato, il metodo esce senza creare
        /// e retituire alcun elemento.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/08/2017 - Ledro - v. 1.0.1.0
        /// Mdfd: 02/09/2017 - Monterotondo - v. 1.0.1.1 - correzione BUG
        /// </pre>
        /// ---- 
        /// </summary>
        /// <param name="p_Name">Nome dell'elemento (not null)</param>
        /// <param name="p_Text">Testo dell'elemento (null)</param>
        /// <param name="p_index">Numero di posizioni dopo l'elemento corrente in cui effettuare l'inserimento.</param>
        /// <returns>L'elemento creato ed inserito.</returns>
        public Spx_XmlElement appendElementAfter(String p_Name, String p_Text, Int16 p_index)
        {
            Spx_XmlElement _current = this;
            Int16 _idx = 0;

            if (p_Name == null)
                return null;
            Spx_XmlElement _toRet = new Spx_XmlElement();
            _toRet.MyName = p_Name;
            _toRet.MyText = p_Text;
            // INSERIMENTO 
            while (_current.MyNext != null && _idx<p_index) // <mdfd @MdN: 02/09/2017 />
            {
                _current = _current.MyNext;
                _idx++;                                     // <add @MdN: 02/09/2017 />    
            }
            _toRet.MyNext = _current.MyNext;
            _current.MyNext = _toRet;
            // BIND            
            _toRet.MyChief = MyChief;
            _toRet.MyFather = MyFather;
            return _toRet;
        }

        /// <summary>
        /// Inserisce l'elemento passato come parametro dietro l'elemento corrente dopo un certo numero di posizioni,
        /// eseguendo correttamente il binding.
        /// <p>
        /// Se il numero di posizioni è 0 (p_index=0) l'elemento viene inserito subito dopo l'elemento corrente.<br></br> 
        /// Se il numero di posizioni eccede il numero di elementi che seguono quello corrente, l'effetto è equivalente 
        /// a quello ottenuto con il metodo appendElement(String p_Name, String p_Text), cioè un semplice accodamento.
        /// </p>
        /// <p>
        /// Se il parametro p_Element non viene specificato, il metodo esce senza restituire alcun elemento.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 22/08/2017 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_Element"></param>
        /// <param name="p_Index"></param>
        /// <returns></returns>
        public Spx_XmlElement appendElementAfter(Spx_XmlElement p_Element, Int16 p_Index)
        {
            Int16 _idx = 0;

            if (p_Element == null)
                return null;

            // INSERIMENTO
            Spx_XmlElement _current = this;
            while (_current.MyNext != null && _idx <= p_Index)
            {
                _current = _current.MyNext;
            }
            p_Element.MyNext = _current.MyNext;
            _current.MyNext = p_Element;
            // BIND            
            p_Element.MyChief = MyChief;
            p_Element.MyFather = MyFather;
            return p_Element;
        }

        /// <summary>
        /// Inserisce l'elemento passato come parametro immediatamente dietro l'elemento corrente
        /// eseguendo correttamente il binding.<br></br>
        /// Equivale a appendElementAfter(Spx_XmlElement p_Element, 0).
        /// <p>
        /// Se il numero di posizioni è 0 (p_index=0) l'elemento viene inserito subito dopo l'elemento corrente.<br></br> 
        /// Se il numero di posizioni eccede il numero di elementi che seguono quello corrente, l'effetto è equivalente 
        /// a quello ottenuto con il metodo appendElement(String p_Name, String p_Text), cioè un semplice accodamento.
        /// </p>
        /// <p>
        /// Se il parametro p_Element non viene specificato, il metodo esce senza restituire alcun elemento.
        /// </p>
        /// ----
        /// @MdN
        /// Crtd: 22/08/2017 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_Element"></param>
        /// <param name="p_Index"></param>
        /// <returns></returns>
        public Spx_XmlElement appendElementAfter(Spx_XmlElement p_Element)
        {
            return appendElementAfter(p_Element, 0);
        }

        /// <summary>
        /// <p>
        /// Crea un nuovo elemento e lo accoda tra gli elementi del livello inferiore,
        /// eseguendo correttamente il binding.
        /// </p>
        /// <p>
        /// Se il parametro p_Name non viene specificato, il metodo esce senza creare
        /// e retituire alcun elemento.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_Name">Nome dell' elemento (not null)</param>
        /// <param name="p_Text">Corpo dell'elemento (ovvero null)</param>
        /// <returns>L'elemento creato ed inserito.</returns>
        public Spx_XmlElement insertElement(String p_Name, String p_Text)
        {
            Spx_XmlElement _current = this;
            Spx_XmlElement _toRet;
            if (this.MyInner != null)
                return this.MyInner.appendElement(p_Name, p_Text);
            else
            {
                //CREAZIONE
                _toRet = new Spx_XmlElement();
                _toRet.MyName = p_Name;
                _toRet.MyText = p_Text;
                //INSERIMENTO AL LIVELLO INFERIORE
                this.MyInner = _toRet;
                //COMPLETAMENTO DEL BINDING
                _toRet.MyChief = _toRet;
                _toRet.MyFather = this;
                return _toRet;
            }

        }



        /// <summary>
        /// <p>
        /// Restituisce il Nome dell'attributo in posizione p_i (base 0).
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/04/2015
        /// </pre>
        /// </summary>
        /// <param name="p_i">indice base 0</param>
        /// <returns>il nome dell'attributo in posizione i ovvero null</returns>
        public String getEntryNameAt(int p_i)
        {
            Spx_ConfigEntry _temp;
            _temp = getEntryAt(p_i);
            
            if (_temp == null)
                return null;
            return _temp.Key;
        }

        /// <summary>
        /// <p>
        /// Restituisce il Valore dell'attributo in posizione p_i (base 0).
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/04/2015
        /// </pre>
        /// </summary>
        /// <param name="p_i">indice base 0</param>
        /// <returns>il valore dell'attributo in posizione i ovvero null</returns>
        public String getEntryValueAt(int p_i)
        {
            Spx_ConfigEntry _temp;
            _temp = getEntryAt(p_i);

            if (_temp == null)
                return null;
            return _temp.Value;
        }

        /// <summary>
        /// <p>
        /// Restituisce il valore dell'attributo passato come argomento, se lo trova,
        /// o null se non lo trova.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 04/04/2015
        /// </pre>
        /// </summary>
        /// <param name="p_Attribute">Nome dell'attributo.</param>
        /// <returns>Il valore dell'attributo o null.</returns>
        public String getValue(String p_Attribute)
        {
            Spx_ConfigEntry _current = this.MyHead;
            if (_current == null)
                return null;
            while (_current != null)
            {
                if (_current.Key.ToLower().CompareTo(p_Attribute.ToLower()) == 0)
                    return _current.Value;
                _current = _current.next;
            }
            return null;
        }//fine

        /// <summary>
        /// <p>
        /// Crea e restituisce un oggetto <b>Writer</b> di una configurazione che consente
        /// di scrivere tale configurazione su un file in formato XML.
        /// </p>
        /// </summary>
        /// <param name="p_path">path del file di destinazione</param>
        /// <returns>L'oggetto writer ovvero null.</returns>
        public Spx_XmlConfigWriter getXmlConfigWriter(String p_path)
        {
            Spx_XmlConfigWriter _toRet = null;
            if (p_path == null)
                return null;
            _toRet = new Spx_XmlConfigWriter(p_path, this);
            return _toRet;
        }

        /// <summary>
        /// <br>Cerca tutti gli elementi che hanno un determinato nome.</br>
        /// <bre>Effettua una visita prima in profondità (Depth-First Search o DFS)</bre>
        /// 
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 13/02/2016 11:32 - Monterotondo.
        /// </pre>
        /// </summary>
        /// <param name="p_ElemName">Elemento dal quale far partire la ricerca.</param>
        /// <returns>Un array di elementi con il nome specificato o null se nessun elemento è stato trovato.</returns>
        public Spx_XmlElement[] searchAllElements(String p_ElemName)
        {
            System.Collections.Generic.List<Spx_XmlElement> _use = null;
            //_tempList = new List<Spx_XmlElement>();
            Spx_XmlElement[] _temp = null;
            Spx_XmlElement[] _toRet = null;
            Spx_XmlElement _current = null;

            // * Controllo
            if (p_ElemName == null)
                return null;

            // Visito me stesso
            if (p_ElemName.CompareTo(this.Name) == 0)
            {
                _toRet = new Spx_XmlElement[1];
                _toRet[0] = this;
            }

            //Visita prima in profondità
            if (this.Inner != null)
            {
                _temp = this.Inner.searchAllElements(p_ElemName);
                if (_temp != null && _temp.Length > 0)
                {
                    if (_toRet != null)
                        _toRet.Concat(_temp);
                    else
                        _toRet = _temp;
                }
                _temp=null;
            }

            //Visita poi in larghezza
            _current = this.Next;
            if(_current != null)
            {
                _temp = _current.searchAllElements(p_ElemName);
                if (_temp != null && _temp.Length > 0)
                {
                    if (_toRet != null)
                        _toRet.Concat(_temp);
                    else
                        _toRet = _temp;
                }
                _temp = null;
               // _current = _current.Next;   //scorrimento
            }
            return _toRet;
        } // fine search all elements


        #endregion

    }
}
