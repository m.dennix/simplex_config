﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMPLEX_Config
{
    /// <summary>
    /// <p>
    /// Classe che si occupa di scrivere su un file fisico una configurazione
    /// creata dinamicamente da codice per una maschera.
    /// </p>
    /// <p>
    /// La classe non ha costruttori pubblici pertanto, per crearne un'istanza,
    /// si deve ricorre al metodo pubblico <see cref="Spx_XmlElement.getWriter()"/> 
    /// della classe <see cref="Spx_XmlElement"/>.
    /// </p>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 03/05/2015
    /// </pre>
    /// </summary>
    public class Spx_XmlConfigWriter
    {

        /// <summary>
        /// Elemento radice della configurazione da salvare.
        /// </summary>
        Spx_XmlElement MyElement = null;
        
        /// <summary>
        /// File di output
        /// </summary>
        String MyFile = null;


        #region costruttori
        
        /// <summary>
        /// Dummy
        /// </summary>
        internal Spx_XmlConfigWriter()
        {
            //Dummy
        }

        /// <summary>
        /// <p>
        /// Crea un oggetto Spx_XmlConfigWriter.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 03/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_file">path del file da creare (not null)</param>
        /// <param name="p_elem">elemento radice della struttura di configurazione (not null)</param>
        /// <remarks>
        /// Lancia un'eccezione generica se
        /// <ul>
        /// <li>uno o entrambi i parametri sono null;</li>
        /// <li>l'elemento passato come parametro non è una radice.</li>
        /// </ul>
        /// </remarks>
        internal Spx_XmlConfigWriter(String p_file, Spx_XmlElement p_elem)
        {
            //controlli
            if(p_file==null || p_elem==null)
            {
                Spx_Message _msg = new Spx_Message();
                _msg.MessageCode = 0;
                _msg.Message = "One or both the parameters are null.";
                throw _msg;
            }

            if (p_elem.MyFather != null)
            {
                Spx_Message _msg = new Spx_Message();
                _msg.MessageCode = 0;
                _msg.Message = "The element must be the 'root'";
                throw _msg;
            }
            MyFile = p_file;
            MyElement = p_elem;
        }

        #endregion

        #region metodi pubblici

        /// <summary>
        /// <p>
        /// Scrive la struttura di configurazione su un file di testo.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 03/05/2015
        /// </pre>
        /// </summary>
        /// <returns><b>True</b> se la scrittura avviene con successo, <b>False</b> altrimenti.</returns>
        public Boolean write()
        {
            String _toWrite = null;
            System.IO.StreamWriter _outfile = null;

            if (System.IO.File.Exists(MyFile) == true)
                return false;

            _toWrite = recursiveWriteElement(MyElement);
            if (_toWrite != null)
            {
                _outfile = new System.IO.StreamWriter(MyFile);
                //INTESTAZIONE
                _outfile.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                //CORPO
                _outfile.WriteLine(_toWrite);
                _outfile.Flush();
                _outfile.Close();
            }
            return true;
        }

        #endregion

        #region metodi privati ed interni


        /// <summary>
        /// <p>
        /// Restituisce una stringa contenente tutti gli elementi XML corrispondenti all'elemento passato
        /// come parametro, compresi gli elementi figli, gli elementi di pari livello ('peer') ed i rispettivi
        /// figli.
        /// </p>
        /// <p>
        /// Sfrutta la ricorsione.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 03/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_elem">Elemento della struttura di configurazione.</param>
        /// <returns>Una stringa.</returns>
        internal String recursiveWriteElement(SIMPLEX_Config.Spx_XmlElement p_elem)
        {
            return recursiveWriteElement(p_elem, 0);
        }

        /// <summary>
        /// <p>
        /// Restituisce una stringa contenente tutti gli elementi XML corrispondenti all'elemento passato
        /// come parametro, compresi gli elementi figli, gli elementi di pari livello ('peer') ed i rispettivi
        /// figli.
        /// </p>
        /// <p>
        /// Sfrutta la ricorsione.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 03/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_elem">Elemento della struttura di configurazione.</param>
        /// <param name="p_level">Livello di identazione. Quando non invocato mediante ricorsione deve essere 0 
        /// (vedi overload con un solo argomento).</param>
        /// <returns>Una stringa.</returns>
        internal String recursiveWriteElement(SIMPLEX_Config.Spx_XmlElement p_elem, int p_level)
        {
            Spx_XmlElement _elem = null;
            System.Text.StringBuilder _sb = null;
            
            //controllo
            if (p_elem == null)
                return null;
            else
                _elem = p_elem;

            //Inizializzazione
            _sb = new StringBuilder();

            //ciclo di esplorazione degli elementi 'peer'
            while (_elem != null)
            {
                for (int _t = 0; _t < p_level; _t++)
                    _sb.Append("\t");
                //APERTURA
                _sb.Append("<").Append(_elem.MyName);
                //ATTRIBUTI
                Spx_ConfigEntry _entry = null;
                _entry = _elem.MyHead;
                while (_entry != null)
                {
                    _sb.Append(" ").Append(_entry.Key).Append("=\"").Append(_entry.Value).Append("\"");
                    _entry = _entry.next;
                }
                _sb.Append(">\n");
                //FIGLI O TESTO
                if (_elem.MyInner != null)
                    _sb.Append(recursiveWriteElement(_elem.MyInner, p_level + 1));
                else
                    if (_elem.MyText != null)
                    {
                        for (int _t = 0; _t < p_level+1; _t++)
                            _sb.Append("\t");
                        _sb.Append(_elem.MyText);
                    }
                //CHIUSURA
                for (int _t = 0; _t < p_level; _t++)
                    _sb.Append("\t");
                _sb.Append("</").Append(_elem.MyName).Append(">\n");
                _elem = _elem.MyNext;
            }
            return _sb.ToString();
        }

        #endregion

    }
}
