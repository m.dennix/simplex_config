﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SIMPLEX_Config
{
    /// <summary>
    /// Classe che rappresenta un file di configurazione.
    /// <pre>
    /// ----
    /// @MdN 
    /// Crtd: 30/11/2013 - v 1.0.0.0
    /// Mdfd: 06/01/2016 - v 1.0.0.2 - correzione bugs in open().
    /// Mdfd: 14/01/2016 - Metodi per l'aggiunta di una Sezione e di una Entry (chiave:=valore)
    /// Mdfd: 23/01/2016 - Introdotta la possibilità di scrittura di un file di configurazione.
    /// Mdfd: 24/01/2016 - ... continua
    /// Mdfd: 15/10/2021 - v.1.0.2.0
    ///                     - introdotta property pubblica SectionNames
    ///                     - introdotta property pubblica Opened
    /// Mdfd: 22/10/2021 - v.1.0.2.1
    ///                     - Metodo Open(): corretta anomalia Issue#3: errore bloccante in presenza 
    ///                       di parentesi angolari in posizioni diverse dalle definizioni delle sezioni.
    ///                       Le sezioni devono essere sempre in prima posizione su una riga del file di configurazione.
    /// </pre>
    /// </summary>
    public class Spx_ConfigFile
    {
        private String l_fname;
        private StreamReader l_sr;
        private Spx_Collection l_Collection;

        private Boolean l_modified = false;

        private const string SPX_COMMENT="##";
        private const char SPX_OPENSECTION = '<';
        private const char SPX_CLOSESECTION = '>';
        private const String SPX_CONTINUE = ".._";
        private const String SPT_DEFINITION = ":=";

        private char[] ToTrim = {' ', '\t', '\n' };

        private String l_Strerror="--- LOG ---\n";

        /// <summary>
        /// True se aperto
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 18/10/2021
        /// </pre>
        /// </summary>
        private Boolean _opened = false;

        #region COSTRUTTORI

        /// <summary>
        /// Costruttore.
        /// Crea solo le strutture interne. Utile se si deve creare 'ex novo' un file di configurazione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2016
        /// </pre>
        /// </summary>
        public Spx_ConfigFile()
        {
            // creazione della Collection
            l_Collection = new Spx_Collection();
        }
        /// <summary>
        /// Costruttore che predispone il flusso di lettura di un file di configurazione.
        /// Il file di configurazione DEVE esistere, altrimenti usare il costruttore senza
        /// argomenti.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 24/01/2016 - Monterotondo - commenti.
        /// Mdfd: 25/04/2016 19:09 - Monterotondo: aggiunta del parametro p_spx_collection. Assembly v. 1.0.0.4
        /// </pre>
        /// </summary>
        /// <param name="p_spx_collection">Collezione di sezioni di coppie Chiave-Valore. Se null viene la collezione viene creata ex-novo.</param>
        /// <param name="p_fname">nome del file di configurazione</param>
        /// <exception cref="Exception">Se il parametro non è valorizzato.</exception>
        /// <exception cref="SIMPLX_Config.Spx_Message">Se il file da leggere non esiste. In questo caso, usare il costruttore senza argomenti.</exception>
        public Spx_ConfigFile(String p_fname, Spx_Collection p_spx_collection)
        {
            // Inizializzazione del flusso di INPUT
            l_fname = p_fname;
            if (p_fname == null)
                throw new Exception("Spc_ConfigFile.Spx_Config(String): Parametro non valorizzato");
            try
            {
                l_sr = new StreamReader(p_fname);
            }
            catch (Exception E)
            {
                Spx_Message EE = new Spx_Message();
                EE.MessageCode = 0;
                EE.Message = "Spc_ConfigFile.Spx_Config(String): Impossibile aprire il file specificato\n" + E.Message.ToString();
                EE.InnerException = E;
                throw EE;
            }

            // creazione della Collection
            if (p_spx_collection == null)                   //@MdN 25/04/2016 19:09
                l_Collection = new Spx_Collection();
            else
                l_Collection = p_spx_collection;            //@MdN 25/04/2016 19:09
        }

        /// <summary>
        /// Costruttore con collezione di Sezioni di coppie Chiave-Valore.
        /// 
        /// <pre>
        /// ----
        /// Introdotta con la versione dell'assembly 1.0.0.4
        /// @MdN
        /// Crtd: 25/04/2016 16:28 Monterotondo 
        /// </pre>
        /// </summary>
        /// <param name="p_spx_collection">Collezione di s</param>
        public Spx_ConfigFile(Spx_Collection p_spx_collection)
        {
            if(p_spx_collection==null)
                l_Collection = new Spx_Collection();
            else
                l_Collection = p_spx_collection;
        }

        /// <summary>
        /// Costruttore che predispone il flusso di lettura di un file di configurazione.
        /// Il file di configurazione DEVE esistere, altrimenti usare il costruttore senza
        /// argomenti.<br></br>
        /// Equivale a Spx_ConfigFile(String p_fname, null).<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 25/04/2016 19:09 
        /// </pre>
        /// </summary>
        /// <param name="p_fname">nome del file di configurazione</param>
        /// <exception cref="Exception">Se il parametro non è valorizzato.</exception>
        /// <exception cref="SIMPLX_Config.Spx_Message">Se il file da leggere non esiste. In questo caso, usare il costruttore senza argomenti.</exception>
        public Spx_ConfigFile(String p_fname)
            : this(p_fname, null)
        {
            //NOP
        }


        



        #endregion

        #region METODI PUBBLICI - PUBLIC METHODS

        /// <summary>
        /// Rimuove la chiave dalla sezione specificata. Restituisce:
        /// <ul>
        /// <li>false: se non esegue alcuna rimozione (ad esempio perchè o la chiave o la sezione non esistono);</li>
        /// <li>true: se esegue la rimozione;</li>
        /// </ul>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/01/2016 - Monterotondo
        /// Mdfd: 23/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_Section">Nome della sezione ovvero null per quella di default</param>
        /// <param name="p_Key">Chiave da rimuovere</param>
        /// <returns>True se esegue la rimozione, False altrimenti.</returns>
        public Boolean removeKey(String p_Section, String p_Key)
        {
            Boolean _toRet;
            if (p_Key == null)
                return false;
            
            _toRet = l_Collection.remove(p_Section, p_Key);
            if(_toRet==true)
                l_modified = true;
            return _toRet;
        }

        /// <summary>
        /// Aggiunge una chiave valorizzata alla sezione specificata. Non inserisce duplicati.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 23/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_Section">Sezione. Se null prende la sezione di default.</param>
        /// <param name="p_Key">Chiave (not null)</param>
        /// <param name="p_Value">Valore (not null)</param>
        /// <returns>True se l'inserimento va a buon fine, False altrimenti.</returns>
        /// <exception cref="SIMPLEX_Config.Spx_Message">Codici 0, 9</exception>
        public Boolean add(String p_Section, String p_Key, String p_Value)
        {
            // * Controlli
            if (p_Key == null)
                return false;
            if (p_Value == null)
                return false;
            try
            {
                l_Collection.add(p_Section, p_Key, p_Value);
            }
            catch (Spx_Message _spxm)
            {
                _spxm.Source = "Spx_ConfigFile.add()-->" + _spxm.Source;
                throw _spxm;
            }
            catch (Exception E)
            {
                Spx_Message _spxm = new Spx_Message();
                String _section;
                if(p_Section==null)
                    _section="default";
                else
                    _section = p_Section;
                _spxm.InnerException = E;
                _spxm.Message = "Errore in Spx_ConfigFile.add(" + _section + ", " + p_Key + ", " + p_Value + "): " + E.Message;
                _spxm.Source = "Spx_ConfigFile.add()-->" + E.Source;
                _spxm.MessageCode = 0;
                throw _spxm;
            }
            l_modified = true;
            return true;
        }

        /// <summary>
        /// Aggiunge una chiave valorizzata alla sezione specificata. 
        /// Se la chiave già esiste, sostituisce il vecchio valore con quello passato per argomento.
        /// Non inserisce duplicati.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 23/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_Section">Sezione. Se null prende la sezione di default.</param>
        /// <param name="p_Key">Chiave (not null)</param>
        /// <param name="p_Value">Valore (not null)</param>
        /// <returns>True se l'inserimento va a buon fine, False altrimenti.</returns>
        /// <exception cref="SIMPLEX.Config.Spx_Message">Codici 0</exception>
        public Boolean addWithReplace(String p_Section, String p_Key, String p_Value)
        {
            // * Controlli
            if (p_Key == null)
                return false;
            if (p_Value == null)
                return false;
            try
            {
                l_Collection.addWithReplace(p_Section, p_Key, p_Value);
            }
            catch (Spx_Message _spxm)
            {
                _spxm.Source = "Spx_ConfigFile.addWithReplace()-->" + _spxm.Source;
                throw _spxm;
            }
            catch (Exception E)
            {
                Spx_Message _spxm = new Spx_Message();
                String _section;
                if (p_Section == null)
                    _section = "default";
                else
                    _section = p_Section;
                _spxm.InnerException = E;
                _spxm.Message = "Errore in Spx_ConfigFile.addWithReplace(" + _section + ", " + p_Key + ", " + p_Value + "): " + E.Message;
                _spxm.Source = "Spx_ConfigFile.addWithReplace()-->" + E.Source;
                _spxm.MessageCode = 0;
                throw _spxm;
            }
            l_modified = true;
            return true;
        }


        /// <summary>
        /// Aggiunge una nuova sezione se non esiste.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/01/2016
        /// Mdfd: 23/01/2016 - Monterotondo - Valore restituito booleano.
        /// </pre>
        /// </summary>
        /// <param name="p_SectionName">Nome della nuova sezione.</param>
        /// <returns>True se la sessione viene aggiunta, False altrimenti</returns>
        public Boolean addSection(String p_SectionName)
        {
            // * Controllo parametro
            if (p_SectionName == null)
                return false;
            // Se NON esiste la sessione, la si crea e la si aggiunge
            if (getKeys(p_SectionName) == null)
            {
                if (l_Collection != null)
                {
                    l_Collection.addSection(p_SectionName);
                    l_modified = true;
                    return true;
                }
            }
            return false;
        }


        /// <summary>
        /// Rimuove, se esiste, la sezione indicata attraverso il nome. 
        /// <strong>Non si può eliminare la sezione di default.</strong>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/01/2016 - Monterotondo
        /// Mdfd: 23/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_SectionName">Nome della sezione da eliminare.</param>
        /// <returns>True, se l'eliminazione è avvenuta, False altrimenti.</returns>
        public Boolean removeSection(String p_SectionName)
        {
            Boolean _toRet = false;

            // * Controllo parametro
            if (p_SectionName == null)
                return _toRet;
               
            // NON SI PUO' ELIMINARE LA SEZIONE DI DEFAULT
            if (p_SectionName.ToLower().CompareTo("default") == 0)
                return _toRet;

            // Se la sessione esiste, la si rimuove
            _toRet = l_Collection.removeSection(p_SectionName);
            if (_toRet == true)
                l_modified = true;
            return _toRet;
        }


        /// <summary>
        /// <p>
        /// Restituisce l'elenco delle chiavi del file di configurazione per una determinata 
        /// sezione. Se la sezione è 'null', restituisce l'elenco delle chiavi della sezione
        /// di default.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN 
        /// Crtd: 26/12/2015 - Livorno
        /// 
        /// </pre>
        /// </summary>
        /// <param name="p_section">Nome della sezione ovvero null per la sezione di default.</param>
        /// <returns>Elenco delle chiavi della sezione specificata.</returns>
        public String[] getKeys(String p_section)
        {
            Spx_Section _tmpSec = null;

            //controlli
            if (l_Collection == null)
                return null;
            
            //prendo la section
            try
            {
                _tmpSec = l_Collection.getSection(p_section);
                if (_tmpSec == null)
                    return null;
            }
            catch (Spx_Message _spxm)
            {
                return null;
            }

            //creo l'array di stringhe
            return _tmpSec.Keys;
        }

        /// <summary>
        /// Scrive la configurazione corrente su un file.
        /// Se il file esiste, viene sovrascritto ed il vecchio file viene salvato con il prefisso 'OLD_'.
        /// Equivale a save(true).
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 23/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        /// <exception cref="SIMPLEX_Config.Spx_Message">Vedi save(p_overwrite)</exception>
        public void save()
        {
            save(true);
        }

        /// <summary>
        /// Scrive la configurazione corrente sul file il cui nome è specificato nell'argomento.
        /// Se il file esiste lo sovrascrive dopo averlo salvato in copia di backup con prefisso 'OLD_'.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_fname">path valido del fine di destinazione.</param>
        /// <exception cref="SIMPLEX_Config.Spx_Message">Vedi save(p_overwrite)</exception>
        public void save(String p_fname)
        {
            l_fname = p_fname;
            save(true);
        }

        /// <summary>
        /// Scrive la configurazione corrente sul file il cui nome è specificato nell'argomento.
        /// Se il file esiste ed il parametro p_overwrite=true, lo sovrascrive dopo averlo salvato in copia di backup con prefisso 'OLD_'.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_fname">path valido del fine di destinazione.</param>
        /// <exception cref="SIMPLEX_Config.Spx_Message">Vedi save(p_overwrite)</exception>
        public void save(String p_fname, Boolean p_overwrite)
        {
            l_fname = p_fname;
            save(true);
        }

        /// <summary>
        /// Scrive la configurazione corrente su un file.
        /// Se il file esiste, viene sovrascritto SOLO SE il parametro p_overwrite è impostato a TRUE. In questo caso il vecchio file viene salvato con il prefisso 'OLD_'.
        /// Nel caso in cui il parametro p_overwrite abbia valore FALSE viene lanciata un'eccezione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 23/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_overwrite">True, il file se esiste viene sovrascritto, previa copia di backup, False altrimenti.</param>
        /// <exception cref="SIMPLEX_Config.Spx_Message">Se il nome del file non viene specificato (10);
        /// Se il file specificato esiste e p_overwrite=false (11); 
        /// Se fallisce la copia di backup del file corrente (0);
        /// Se fallisce la scrittura del file (12).
        /// </exception>
        public void save(Boolean p_overwrite)
        {
            StreamWriter _sw = null;
            // * Controllo se la configurazione ha subito midifiche
            if (l_modified == false)
                return;
            if (l_fname == null)
            {
                Spx_Message _spxm = new Spx_Message();
                _spxm.Message = "Nome del file non specificato";
                _spxm.MessageCode = 10;
                _spxm.Source = "Spx_ConfigFile.save()";
                throw _spxm;
            }
            // Se il file esiste vi sono due possibilità:
            // 1) o si esce con una eccezione
            // 2) o si sovrascrive il file esistente che verrà salvato in copia di backup con prefisso 'OLD_'
            if (System.IO.File.Exists(l_fname) == true)
            {
                if (p_overwrite == false)
                {
                    Spx_Message _spxm = new Spx_Message();
                    _spxm.Message = "Nome del file Esistente";
                    _spxm.MessageCode = 11;
                    _spxm.Source = "Spx_ConfigFile.save()";
                    throw _spxm;
                }
                else
                {
                    try
                    {
                        String[] _components = null;
                        String[] _separators = { "\\", "/" };
                        String _path = null;
                        String _name = null;
                        int _lastidx = 0;
                        
                        if (l_fname.Contains("\\") == true || l_fname.Contains("/"))
                        {
                            _lastidx = l_fname.LastIndexOf('\\');
                            if (_lastidx > 0)
                            {
                                _path = l_fname.Substring(0, _lastidx);
                                _name = l_fname.Substring(_lastidx+1,(l_fname.Length - (_lastidx + 1)));
                            }
                        }
                        if (_path != null)
                            _path = _path + "\\";
                        System.IO.File.Copy(l_fname, _path  + "OLD_" + _name, true);
                        System.IO.File.Delete(l_fname);
                    }
                    catch (Exception E)
                    {
                        Spx_Message _spxm = new Spx_Message();
                        _spxm.Message = "Errore nella copia del file da sostituire: " + E.Message;
                        _spxm.MessageCode = 0;
                        _spxm.Source = "Spx_ConfigFile.save()-->" + E.Source;
                        _spxm.InnerException = E;
                        throw _spxm;
                    }
                }
            }
            // scrittura del file
            try
            {
                if (l_fname != null && System.IO.File.Exists(l_fname) == false)
                {
                    _sw = new StreamWriter(l_fname, false);
                    if (_sw != null)
                    {
                        Spx_Section _tmp = null;
                        Int16 _idx = 0;
                        String _toWrite = null;

                        // Intestazione
                        _toWrite = SPX_COMMENT + " File " + l_fname;
                        _sw.WriteLine(_toWrite);
                        _toWrite = SPX_COMMENT + " generato il: " + System.DateTime.Now;
                        _sw.WriteLine(_toWrite);
                        _toWrite = SPX_COMMENT + " con Simplex Config Library (c) Michele de Nittis 2013-2016";
                        _sw.WriteLine(_toWrite);
                        _toWrite = SPX_COMMENT + SPX_COMMENT + SPX_COMMENT + SPX_COMMENT + SPX_COMMENT;
                        _sw.WriteLine(_toWrite);
                        // prima si scrive la sezione di default
                        _tmp = l_Collection.getSection(null);
                        String[] _keys = null;
                        String _val;
                        if (_tmp != null)
                        {
                            _keys = _tmp.Keys;
                            foreach (String _s in _keys)
                            {
                                _val = _tmp.searchFor(_s);
                                _toWrite = _s + SPT_DEFINITION + _val;
                                _sw.WriteLine(_toWrite);
                            }
                        }
                        // Poi si scrivono tutte le altre
                        for (_idx = 0; _idx < l_Collection.Sections; _idx++)
                        {
                            _tmp = l_Collection.getSectionAt(_idx);
                            if (_tmp != null)
                            {
                                if (_tmp.name.ToLower().CompareTo("default") == 0)
                                    continue; // si salta perchè è già stata scritta
                                _toWrite = SPX_OPENSECTION + _tmp.name + SPX_CLOSESECTION;
                                _sw.WriteLine(_toWrite);
                                _keys = _tmp.Keys;
                                if (_keys != null)
                                {
                                    foreach (String _s in _keys)
                                    {
                                        _val = _tmp.searchFor(_s);
                                        _toWrite = _s + SPT_DEFINITION + _val;
                                        _sw.WriteLine(_toWrite);
                                    }
                                }
                            }
                        } // fine ciclo scrittura di tutte le altre sezioni
                    }   // fine if interno
                    else
                    {
                        Spx_Message _spxm = new Spx_Message();
                        _spxm.Message = "Il file  " + l_fname + " non può essere creato nella posizione specificata";
                        _spxm.MessageCode = 13;                        
                        _spxm.Source = "Spx_ConfigFile.save()";
                        throw _spxm;
                    }
                } // fine if esterno
                }
                catch (Exception E)
                {
                    Spx_Message _spxm = new Spx_Message();
                    _spxm.Message = "Errore nella scrittura del file " + l_fname;
                    _spxm.MessageCode = 12;
                    _spxm.InnerException = E;
                    _spxm.Source = "Spx_ConfigFile.save()-->" + E.Source;
                    throw _spxm;
                }
                finally
                {
                    if (_sw != null)
                    {
                        _sw.Flush();
                        _sw.Close();
                    }
                    //_fw.Flush();
                    //_fw.Close();
                }
            }// fine metodo


        /// <summary>
        /// Apre il file e ne esegue il parsing.<br></br>
        /// Controllare lo stato di apertura (vedi propr. Opened) prima di aprire.<br></br>
        /// <PRE>
        /// ----
        /// @MdN 
        /// Crtd: 03/12/2013 --> 04/12/2013
        /// Mdfd: 05/01/2016 Monterotondo - chiusura dello stream al termine delle operazioni di parsing - v.1.0.0.1
        /// Mdfd: 18/10/2021 Roma         - Imposta _opened=true al termine - v.1.0.2.0.
        /// </PRE>
        /// </summary>
        /// <exception cref="Spx_Message">(0)</exception>
        public void open()
        {
            String tmpStr = null;               //stringa di lavoro
            String CurrentSection = null;       //riferimento alla sezione corrente
            String CurrentKey = null;           //riferimento alla chiave corrente
            String CurrentValue = null;         //riferimento al valore corrente
            Boolean continued = false;          //continuazione su più righe del valore.
            int l_length = 0;                  //variabile di appoggio per il calcolo della lunghezza di una stringa.
            long ProcessedRow = 0;                //riga in fase di elaborazione
            int NErrors = 0;                        //numero di errori commessi.
            char[] _TrimChars = {' ', '\n', '\t'};


            // * Controlli
            if (l_sr == null)   //<modified @MdN: 24/01/2016>
            {
                if (l_fname == null)
                {
                    Spx_Message E = new Spx_Message();
                    E.Message = "Spx_ConfigFile.open(): The name of the file is not specified.";
                    E.MessageCode = 8;
                    throw E;
                }
            }
            else //<added @MdN: 24/01/2016>
            {   // Se il Reader non esiste significa che è stato usato un costruttore senza argomenti.
                // Istanziamo il reader.
                try
                {
                    l_sr = new StreamReader(l_fname);
                }
                catch (Exception E)
                {
                    Spx_Message EE = new Spx_Message();
                    EE.MessageCode = 0;
                    EE.Message = "Spx_ConfigFile.open(String): unable to open the specified file.\n" + E.Message.ToString();
                    EE.InnerException = E;
                    throw EE;
                }
            }//</added @MdN: 24/01/2016>

            try
            {
                while (l_sr.EndOfStream == false)
                {
                    int idxstart = 0;
                    int idxend = 0;
                    ProcessedRow++;

                    //Processa una riga alla volta
                    try
                    {
                        tmpStr = l_sr.ReadLine();
                    }
                    catch (Exception E)
                    {
                        Spx_Message EE = new Spx_Message();
                        EE.InnerException = E;
                        EE.MessageCode = 0;
                        EE.Message = "Spc_ConfigFile.open():" + E.Message;
                        throw EE;
                    }

                    // ignora i commenti
                    tmpStr = tmpStr.TrimStart(ToTrim);
                    if (tmpStr.StartsWith(SPX_COMMENT))
                        continue;
                    idxstart = tmpStr.IndexOf(SPX_COMMENT, 0);
                    if (idxstart >= 0)
                    {
                        //trovato un commento nella riga, togliamolo!
                        tmpStr = tmpStr.Substring(0, idxstart - 1);
                    }
                    idxstart = 0;   //RESET 
                    // *******
                    // LA RIGA E' ORA PRONTA PER ESSERE ELABORATA!!!
                    // *******
                    // CASO 1: RIGA DI CONTINUAZIONE
                    if (continued == true)
                    {
                        //la riga è una continuazione di una riga precedente e
                        //deve essere concatenata con essa.
                        idxstart = 0;
                        if (tmpStr.EndsWith(SPX_CONTINUE) == true)
                        {
                            //VI E' ANCORA UNA CONTINUAZIONE
                            continued = true;
                            l_length = tmpStr.Length - SPX_CONTINUE.Length;
                        }
                        else
                        {
                            continued = false; //determina l'inserimento di valore-chiave
                            l_length = tmpStr.Length;

                        }
                        //prendo il valore di chiave
                        if (CurrentValue != null)
                            CurrentValue = String.Concat(CurrentValue, (tmpStr.Substring(idxstart, l_length)).Trim(ToTrim));
                        else
                            CurrentValue = tmpStr.Substring(idxstart, l_length).Trim(ToTrim);
                        //SE devo continuare
                        if (continued == false)
                        {
                            l_Collection.add(CurrentSection, CurrentKey, CurrentValue); //voce aggiunta
                            CurrentKey = null;
                            CurrentValue = null;
                        }
                        continue;
                    } //Fine caso 1:

                    //CASO 2: Verifichiamo che si tratti di una sezione
                    String _appoggio = tmpStr;                                          //<add @MdN: v.1.0.2.1 22/10/2021>
                    tmpStr = tmpStr.Trim();                                             //<add @MdN: v.1.0.2.1 22/10/2021>
                    idxstart = tmpStr.IndexOf(SPX_OPENSECTION, 0);
                    //if (idxstart >= 0) //Possibile sezione                            //<del @MdN: v.1.0.2.1 22/10/2021>
                    if (idxstart == 0) //Possibile sezione                              //<add @MdN: v.1.0.2.1 22/10/2021>
                    {
                        //trovato! Forse si tratta di una sessione
                        idxend = tmpStr.IndexOf(SPX_CLOSESECTION, idxstart);
                        if (idxend >= 0)
                        {
                            tmpStr = tmpStr.Substring(idxstart + 1, idxend - idxstart - 1).Trim(ToTrim);
                            CurrentSection = tmpStr;
                            try
                            {
                                l_Collection.addSection(CurrentSection);
                            }
                            catch (Spx_Message E)
                            {
                                //Il codice di errore restituito da addSection è 5
                                l_Strerror = l_Strerror + "\n" + ">>" + ProcessedRow + ", \t" + E.Message;
                                NErrors++;
                            }
                            idxend = 0;
                            idxstart = 0;
                            continue;
                        }
                        else
                        {
                            //idxend<0 --> Errore!!! MANCA LA CHIUSURA DEL TAG DELLA SEZIONE
                            l_Strerror = l_Strerror + "\n" + ">>" + ProcessedRow + ", \t" + "Sessione definita senza carattere di chiusura.";
                            NErrors++;
                            //L'ERRORE SCARTA LA RIGA E PASSA ALLA SUCCESSIVA
                            CurrentKey = null;
                            CurrentValue = null;
                            continued = false;
                            continue;
                        }
                    } // fine possibile sezione
                    
                    tmpStr = _appoggio;     //<add: @MdN v.1.0.2.1 27/10/2021>

                    // NON SI TRATTA DI UNA SESSIONE, QUINDI SI TRATTA DI UNA COPPIA CHIAVE:=VALORE OVVERO DI UNA CONTINUAZIONE.
                    if (tmpStr.Contains(SPT_DEFINITION) == true)
                    {
                        //COPPIA CHIAVE:=VALORE
                        l_length = 0;
                        if (continued == true)
                        {   //CAUTELATIVA!!!
                            //ERRORE! NON PUO' ESISTERE UNA CONTINUAZIONE SE LA RIGA CORRENTE E' UNA COPPIA CHIAVE-VALORE
                            l_Strerror = l_Strerror + "\n" + ">>" + ProcessedRow + ", \t" + "ERRORE! NON PUO' ESISTERE UNA CONTINUAZIONE SE LA RIGA CORRENTE E' UNA COPPIA CHIAVE-VALORE\n";
                            NErrors++;
                            // Si ignora la "continuazione"
                            continued = false;
                        }
                        idxstart = 0;
                        idxend = 0;
                        idxend = tmpStr.IndexOf(SPT_DEFINITION);
                        //Prendo la chiave
                        CurrentKey = tmpStr.Substring(idxstart, idxend).Trim(ToTrim);
                        idxstart = idxend + SPT_DEFINITION.Length;
                        //Prendo il valore
                        if (tmpStr.EndsWith(SPX_CONTINUE) == true)
                        {
                            //VI E' UNA CONTINUAZIONE AL TERMINE DELLA RIGA DEL VALORE.
                            continued = true;
                            idxstart = idxend + SPT_DEFINITION.Length;
                            l_length = tmpStr.Length - idxstart - SPX_CONTINUE.Length;
                            if (CurrentValue != null)
                                CurrentValue = String.Concat(CurrentValue, tmpStr.Substring(idxstart, l_length).Trim(ToTrim));
                            else
                                CurrentValue = tmpStr.Substring(idxstart, l_length).Trim(ToTrim);
                            l_length = 0;
                        }
                        else
                        {
                            continued = false;
                            idxstart = idxend + SPT_DEFINITION.Length;
                            l_length = tmpStr.Length - idxstart;
                            if (CurrentValue != null)
                                CurrentValue = String.Concat(CurrentValue, tmpStr.Substring(idxstart, l_length).Trim(ToTrim));
                            else
                                CurrentValue = tmpStr.Substring(idxstart, l_length).Trim(ToTrim);
                            l_length = 0;
                            this.l_Collection.add(CurrentSection, CurrentKey, CurrentValue);
                            CurrentKey = null;
                            CurrentValue = null;
                        }
                    }
                    else
                    {
                        // NON CONTIENE UNA DEFINIZIONE CHIAVE-VALORE, UNA SEZIONE, UNA CONTINUAZIONE -->SALTA.
                        CurrentValue = null;
                        idxend = 0;
                        idxstart = 0;
                        //MdN 26/03/2016 11:05 Livorno: segnalo anomalia solo se la stringa contiene caratteri diversi da \n.
                        String _teststr = tmpStr.Trim();
                        if (_teststr.Trim(_TrimChars).Length > 0)
                        {
                            l_Strerror = l_Strerror + "\n" + ">>" + ProcessedRow + ", \t" + "ATTENZIONE!!! RIGA SALTATA!\n";
                            NErrors++;
                        }
                        continue;
                    }
                }//CICLO WHILE
            }
            catch (Exception E)
            {
                l_Strerror = l_Strerror + "\n" + ">>" + ProcessedRow + ", \t" + E.Message + "\n";
                NErrors++;
            }
            finally
            {
                l_sr.Close();       //<added @MdN: 03/05/2016>
                _opened = true;     //<added @MdN: 18/10/2021>
            }
            //Lancio dell'eccezione.
            //if (l_Strerror != null)   //<removed @MdN: 06/01/2016>
            if (NErrors > 0)            //<added @MdN: 06/01/2016>
            {
                l_sr.Close();   //<added @MdN: 05/01/2016>
                Spx_Message EE = new Spx_Message();
                EE.MessageCode = 0;
                EE.Message = "Sono stati commessi " + NErrors + " errori.\n" + l_Strerror;
                throw EE;
            }
            //l_sr.Close(); //<added @MdN: 05/01/2016>
        }        

        /// <summary>
        /// Ottiene il valore corrispondente alla chiave specificata all'interno della sezione indicata.
        /// @MdN - 04/12/2013.
        /// </summary>
        /// <param name="p_Section">Nome della sezione. Il valore null corrisponde a "default".</param>
        /// <param name="p_Key">Chiave di ricerca.</param>
        /// <returns>Il valore corrispondente a Sezione/Chiave.</returns>
        public String getValue(String p_Section, String p_Key)
        {
            if (l_Collection != null)
            {
                return l_Collection.getValue(p_Section, p_Key);
            }
            else
            {
                Spx_Message EE = new Spx_Message();
                EE.MessageCode = 0;
                EE.Message = "Spx_ConfigFile.getValue(): Impossibile continuuare in quanto la collezione Spx_Collectio non è istanziata.";
                throw EE;
            }
        }

        /// <summary>
        /// REstituisce una stringa che descrive il contenuto del file di configurazione
        /// </summary>
        /// <returns>Una stringa che descrive il contenuto del file di configurazione</returns>
        /// <exception cref="Spx_Message">Vari possibili errori/messaggi.</exception>
        public String toString()
        {
            String toReturn = null;
            try
            {
                toReturn = this.l_Collection.toString();
            }
            catch (Spx_Message E)
            {
                E.Message = "Spx_ConfigFile.toString():" + E.Message;
                throw E;
            }
            return toReturn;
        }

        #endregion

        #region PROPERTIES PUBBLICHE

        /// <summary>
        /// True se il file di configurazone è già stato caricato/aperto, False altrimenti.<br></br>
        /// Controllare lo stato di apertura prima di eseguire Open.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 18/10/2021
        /// </pre>
        /// </summary>
        public Boolean Opened
        {
            get
            {
                return _opened;
            }
        }


        /// <summary>
        /// Restituisce il nome del file o null se non è stato impostato.
        /// <pre>
        /// ----
        /// @MdN
        /// crtd: 24/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        public String FileName
        {
            get
            {
                return l_fname;
            }
        }

        /// <summary>
        /// Restituisce un array di stringhe contenenti i nomi di tutte 
        /// le sezioni del file di configurazione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 15/10/2021 v.1.0.2.0
        /// </pre>
        /// </summary>
        public String[] SectionNames
        {
            get
            {
                int _t = 0;
                System.Collections.Generic.List<String> _ay = null;

                if (l_Collection == null)
                    return null;

                if (l_Collection.Sections <= 0)
                    return null;

                for (_t = 0; _t < l_Collection.Sections; _t++)
                {
                    if (_ay == null)
                        _ay = new System.Collections.Generic.List<String>();
                    else
                    {
                        if(l_Collection.getSectionAt((short)_t).name!=null)
                            _ay.Add(l_Collection.getSectionAt((short)_t).name);
                            //Append(l_Collection.getSectionAt((short)_t).name);
                    }
                }
                return _ay.ToArray();
            }
        }


        #endregion

        #region METODI PROTETTI E PRIVATI
        #endregion



    }
}
