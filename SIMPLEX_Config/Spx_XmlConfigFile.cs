﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace SIMPLEX_Config
{
    /// <summary>
    /// È una classe molto semplice che, con il solo metodo statico load(), 
    /// consente di caricare la struttura di configurazione dal file XML il 
    /// cui path viene passato in argomento.
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: ????
    /// Mdfd: 01/01/2016: aggiunta questa descrizione.</pre>
    /// </summary>
    public class Spx_XmlConfigFile
    {
        /// <summary>
        /// <p>
        /// Carica un file XML e ne restituisce la truttura.
        /// </p>
        /// </summary>
        /// <param name="p_XmlPath">path del file xml da caricare</param>
        /// <returns></returns>
        public static Spx_XmlElement load(String p_XmlPath)
        {
            Spx_XmlElement _root = null;
            Spx_XmlElement _current = null;
            Spx_XmlElement _temp = null;

            //controlli
            if (p_XmlPath == null)
                return null;
            System.Xml.XmlTextReader _reader = new System.Xml.XmlTextReader(p_XmlPath);
            // ciclo principale di lettura
            while (_reader.Read())
            {
                switch (_reader.NodeType)
                {
                    case System.Xml.XmlNodeType.Element:
                        {
                            _temp = new Spx_XmlElement();
                            _temp.MyName = _reader.Name;

                            if (_root == null)
                            {
                                _root = _temp;          //_temp diventa il nodo radice della gerarchia
                                _current = _temp;       //_temp diventa il nodo corrente
                                if(_reader.IsEmptyElement==false)
                                    _current._opened = true;
                            }
                            else
                            {
                                if (_current._opened == true)
                                {
                                    //NODO DI LIVELLO INFERIORE: aggancio
                                    _temp.MyFather = _current;                                    
                                    _temp.MyNext = null;
                                    _temp.MyInner = null;
                                    _temp.MyChief = _temp;
                                    _current.MyInner = _temp;
                                    //---------
                                    _current = _temp;
                                    if (_reader.IsEmptyElement == false)
                                        _current._opened = true;
                                }
                                else
                                {
                                    //NODO DI PARI LIVELLO: aggancio
                                    _temp.MyFather = _current.MyFather;
                                    _temp.MyNext = null;
                                    _temp.MyInner = null;                                    
                                    _temp.MyChief = _current.Chief;
                                    _current.MyNext = _temp;
                                    //---------
                                    _current = _temp;
                                    if (_reader.IsEmptyElement == false)
                                        _current._opened = true;
                                }
                            }
                            /* ************************************
                               CICLO DI LETTURA DEGLI ATTRIBUTI
                             *********************************** */
                            if (_reader.HasAttributes == true)
                            {
                                int t = 0;
                                _reader.MoveToFirstAttribute();
                                do
                                {
                                    _temp.Add(_reader.Name, _reader.Value);
                                    t++;
                                } while (_reader.MoveToNextAttribute());
                            }

                            break;
                        }
                    case XmlNodeType.EndElement:
                        {
                            if (_current._opened == false)      //se già chiuso, ... 
                                _current = _current.MyFather;   //risale di un livello e ...
                            _current._opened = false;           //Chiude.                            
                            break;
                        }
                    case XmlNodeType.Text:
                        {
                            _current.MyText = _reader.Value;
                            break;
                        }
                    default:
                        break;
                }//fine switch
            } // fine ciclo principale
            _reader.Close();
            return _root;
        }//fine

    }

}
