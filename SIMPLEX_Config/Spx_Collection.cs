﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMPLEX_Config
{
    /// <summary>
    /// Classe che rappresenta il contenuto di un file di configurazione in termini di nome=valore, articolate in sezioni.
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: ??? forse 28/11/2015 ???
    /// Mdfd: 26/12/2015. Da private a public
    /// Mdfd: 06/01/2016 - v. 1.0.0.2 - rimosso codice bug getValue(String, String)
    /// </pre>
    /// </summary>
    /// <seealso cref="Spx_Section"/>
    public class Spx_Collection
    {
        private System.Collections.ArrayList l_Spx_Sections;

        #region METODI E PROPERTIES PRIVATI
        /// <summary>
        /// Cerca la sezione di nome uguale a quella della chiave specificata come prametro.
        /// <pre>
        /// ----
        /// @MdN 
        /// Crtd: 29/11/2013.
        /// Mdfd: 26/12/2015. Da private a protected . V.1.0.0.1
        /// </pre>
        /// </summary>
        /// <param name="p_Name">Chiave della sezione da selezionare. Null equivale alla sezione 
        /// di default.</param>
        /// <returns>La sezione cercata ovvero null se la sezione non esiste.</returns>
        /// <exception cref="Spx_Message (6)(7)"> Attraverso un'eccezione restituisce:
        /// 6. Collezione senza sezioni (neanche quella di default)
        /// 7. La sezione specificata non esiste.
        /// </exception>
        internal Spx_Section getSection(String p_Name)
        {
            Spx_Section tmp = null;
            if (l_Spx_Sections == null || l_Spx_Sections.Count == 0)
            {
                Spx_Message EE = new Spx_Message();
                EE.MessageCode = 6;
                EE.Message = "Spx_Collection.getSection(): Empty collection.";
            }
            //SE non specificato alcun nome, restituisce l'elemento in posizione 0 che 
            //corrisponde alla sezione di default.
            if (p_Name == null)
                return (Spx_Section)l_Spx_Sections[0];
            for (int t = 0; t < l_Spx_Sections.Count; t++)
            {
                tmp = (Spx_Section)l_Spx_Sections[t];
                if (tmp.name.CompareTo(p_Name) == 0)
                    return tmp;
            }
            Spx_Message E = new Spx_Message();
            E.MessageCode = 7;
            E.Message = "Spx_Collection.getSection(): section [" + p_Name + "] doesn't exists.";
            throw E;
        }//FINE
        #endregion

        #region METODI E PROPERTIES PUBBLICI
        /// <summary>
        /// Costruttore pubblico.
        /// @MdN - 28/11/2013
        /// </summary>
        public Spx_Collection()
        {
            l_Spx_Sections = new System.Collections.ArrayList(8);
            //CReazione ed aggiunta della sezione di default
            try
            {
                l_Spx_Sections.Add(Spx_Section.createSection("default"));
            }
            catch (Spx_Message m)
            {
                m.Message = String.Concat("Spx_Collection.Spx_Collection():", m.Message);
                throw m;
            }
        }

        /// <summary>
        /// Restituisce (RO) il numero delle sezioni contenute (compresa quella di default)
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 23/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        public Int16 Sections
        {
            get
            {
                return (Int16)l_Spx_Sections.Count;
            }
        }
        
        /// <summary>
        /// Restituisce la sezione nella posizione specificata.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 23/01/2016 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_idx">Indice della sezione da prelevare</param>
        /// <returns>La sezione desiderata o null in caso di indice inadeguato</returns>
        public Spx_Section getSectionAt(Int16 p_idx)
        {
            if (p_idx < 0)
                return null;
            if (p_idx > l_Spx_Sections.Count)
                return null;
            return (Spx_Section)l_Spx_Sections[p_idx];
        }

        /// <summary>
        /// Restituisce il valore corrispondente alla chiave specificata, nella 
        /// sezione idicata.
        /// Se la sezione è null la ricerca viene condotta nella sezione di default.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 04/12/2013
        /// Mdfd: 06/01/2016 - v. 1.0.0.2 - rimosso codice bug getSection(String, String)
        /// </pre>
        /// 
        /// </summary>
        /// <param name="p_Section">Nome della sezione o null (default)</param>
        /// <param name="p_Key">Chiave</param>
        /// <returns>il valore corrispondente alla sezione ed alla chiave specificata.</returns>
        /// <exception cref="Spx_Message">(2); (3); (4).</exception>
        public String getValue(String p_Section, String p_Key)
        {
            String l_value = null;
            Spx_Section l_section = null;

            //<removed @MdN: 06/01/2016>
            //if (p_Section == null)
            //    p_Section = "default";
            //</removed @MdN: 06/01/2016>

            //Effettua il bubbleing dell'eccezione.
            try
            {
                l_section = this.getSection(p_Section);
            }
            catch (Spx_Message E)
            {
                E.Message = "Spx_Collection.getValue():" + E.Message;
                throw E;
            }
            if (l_section != null)
            {
                //Effettua il bubbleing dell'eccezione.
                try
                {
                    l_value = l_section.searchFor(p_Key);
                }
                catch(Spx_Message E)
                {
                    E.Message = "Spx_Collection.getValue():" + E.Message;
                    throw E;
                }
            }
            return l_value;
        }

        /// <summary>
        /// Rimuove, se esiste, la sezione specificata attraverso il nome. Se la sezione non esiste, il metodo non compie alcuna modifica.
        /// <pre>
        ///----
        ///@MdN
        ///Crtd: 14/01/2016 - Monterotondo
        ///Mdfd: 23/01/2016 - Monterotondo - introdotto il valore restituito booleano; Introdotta la property Sections; Introdotto il metodo getSectionAt().
        ///</pre>
        /// </summary>
        /// <param name="p_SectionName">Nome della sezione da eliminare</param>
        /// <returns>True se la rimozione è avvenuta, False altrimenti.</returns>
        public Boolean removeSection(string p_SectionName)
        {
            Spx_Section _tmpSec = null;
            int _t;

            // * controllo
            if (l_Spx_Sections == null)
                return false;

            // ciclo di scansione dell'array di sezioni
            for (_t = 0; _t < l_Spx_Sections.Count; _t++)
            {
                _tmpSec = (Spx_Section)l_Spx_Sections[_t];
                if (_tmpSec.name.CompareTo(p_SectionName) == 0)
                {
                    l_Spx_Sections.Remove((Spx_Section)_tmpSec); // Rimuove l'intera sezione !!!                
                    return true;
                }                
            }
            return false;
        }


        /// <summary>
        /// Crea ed aggiunge una nuova sezione.
        /// @MdN - 28/11/2013
        /// </summary>
        /// <param name="p_name">Nome della sezione. Se null cerca di aggiungere "default".</param>
        /// <exception cref="Spx_Message (5)">Restituisce un'eccezione nei seguenti casi:
        /// 5. Nome della sezione già esistente.
        /// 
        /// </exception>
        public void addSection(String p_name)
        {
            Spx_Section tmp;
            Spx_Message e;
            Boolean trovato;
            if (p_name == null)
                p_name = "default";
            // Verifica dell'esistenza: se già esiste una sezione con lo stesso nome, non ne crea una nuova.
            trovato = false;
            for (int t = 0; t < l_Spx_Sections.Count; t++)
            {
                tmp = (Spx_Section)l_Spx_Sections[t];
                if (tmp.name.CompareTo(p_name) == 0)
                {
                    e = new Spx_Message();
                    e.MessageCode = 5;
                    e.Message = "Spx_Collection.addSection()" + p_name + " already exists.";
                    throw e;
                }
            }
            //Non trovata --> Aggiungere
            try
            {
                tmp = Spx_Section.createSection(p_name);
            }
            catch (Spx_Message E)
            {
                E.Message = "Spx_Collection.addSection():" + E.Message;
                throw E;
            }
            l_Spx_Sections.Add(tmp);
        }//FINE

        /// <summary>
        /// Compone una stringa con il contenuto della collection.
        /// @MdN - 05/12/2013
        /// </summary>
        /// <returns>Una stringa che rappresenta l'intera collezione.</returns>
        public String toString()
        {
            Spx_Section l_tmp = null;
            StringBuilder toReturn= new StringBuilder();
            try
            {
                for (int t = 0; t < this.l_Spx_Sections.Count; t++)
                {
                    l_tmp = (Spx_Section)l_Spx_Sections[t];
                    toReturn.Append(l_tmp.toString());
                }
            }
            catch (Exception E)
            {
                Spx_Message EE = new Spx_Message();
                EE.InnerException = E;
                EE.MessageCode = 0;
                EE.Message = "Spx_Collection.toString():" + E.Message;
                throw EE;
            }
            return toReturn.ToString();
        }

        /// <summary>
        /// Rimuove, se esiste, la chiave specificata (p_Key) dalla sezione specificata (p_Section).
        /// ----
        /// @MdN
        /// Crtd: 23/01/2016 - Monterotondo
        /// </summary>
        /// <param name="p_Sezione">Nome della sezione o null per la sezione di default.</param>
        /// <param name="p_Key">Chiave da rimuovere.</param>
        public Boolean remove(String p_Sezione, String p_Key)
        {
            // * Controllo
            if (p_Key == null)
                return false;

            Spx_Section _tmp = getSection(p_Sezione);
            // * Controllo
            if (_tmp == null)
                return false;
            // Rimozione
            return _tmp.remove(p_Key);            
        }

        /// <summary>
        /// Aggiunge una coppia chiave-valore ad una sezione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 30/11/2013
        /// Mdfd: 23/01/2016
        /// </pre>
        /// </summary>
        /// <param name="p_Sezione">Nome della sezione. null equivale a default.</param>
        /// <param name="p_Key">Chiave</param>
        /// <param name="p_Value">Valore</param>
        /// <exception cref="Spx_Message (0)">Lancia l'eccezione 0.</exception>
        public void add(String p_Sezione, String p_Key, String p_Value)
        {
            Spx_Section tmp;

            if (p_Sezione == null)
                p_Sezione = "default";

            //(1) Cerca la sezione
            try
            {
                tmp = getSection(p_Sezione);
            }
            catch (Spx_Message E)
            {
                // se non trova la sezione, la crea e la aggiunge.
                try
                {
                    if (E.MessageCode == 6)
                        this.addSection("default");
                    this.addSection(p_Sezione);
                    tmp = getSection(p_Sezione);
                }
                catch (Spx_Message EE)
                {
                    Spx_Message EEE = new Spx_Message();
                    EEE.InnerException = (Spx_Message)EE;
                    EEE.Message = "Spx_Collection.add(): incoherent key [" + p_Sezione + "].";
                    EEE.MessageCode = 0;
                    throw EEE;
                }
            }
            // (2) trovata la sezione, aggiunge una nuova coppia chiave-valore
            try
            {
                tmp.add(p_Key, p_Value);
            }
            //<added @MdN: 23/01/2016>
            catch (Spx_Message _spxm)
            {
                _spxm.Source = "Spx_Collection.add()-->" + _spxm.Source;
            }
            //</added @MdN: 23/01/2016>
            catch (Exception E)
            {
                Spx_Message EE = new Spx_Message();
                EE.InnerException = E;
                EE.MessageCode = 0;
                EE.Message = "Spx_Collection.add():Errore generico nell'inserimento di (" + p_Key + ", " + p_Value + ") in " + p_Sezione + ". Vedi inner Exception";
                throw EE;
            }
        }

                /// <summary>
        /// Aggiunge una coppia chiave-valore ad una sezione. Se la chiave già esiste, ne sostituisce il valore con quello passato per parametro (p_Value).
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 23/01/2016 - Momterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_Sezione">Nome della sezione. null equivale a default.</param>
        /// <param name="p_Key">Chiave</param>
        /// <param name="p_Value">Valore</param>
        /// <exception cref="Spx_Message (0)">Lancia l'eccezione 0.</exception>
        public void addWithReplace(String p_Sezione, String p_Key, String p_Value)
        {
            Spx_Section tmp;

            if (p_Sezione == null)
                p_Sezione = "default";

            //(1) Cerca la sezione
            try
            {
                tmp = getSection(p_Sezione);
            }
            catch (Spx_Message E)
            {
                // se non trova la sezione, la crea e la aggiunge.
                try
                {
                    if (E.MessageCode == 6)
                        this.addSection("default");
                    this.addSection(p_Sezione);
                    tmp = getSection(p_Sezione);
                }
                catch (Spx_Message EE)
                {
                    Spx_Message EEE = new Spx_Message();
                    EEE.InnerException = (Spx_Message)EE;
                    EEE.Message = "Spx_Collection.add(): incoherent key [" + p_Sezione + "].";
                    EEE.MessageCode = 0;
                    throw EEE;
                }
            }
            // (2) trovata la sezione, aggiunge una nuova coppia chiave-valore
            try
            {
                tmp.addWithReplace(p_Key, p_Value);
            }
            catch (Exception E)
            {
                Spx_Message EE = new Spx_Message();
                EE.InnerException = E;
                EE.MessageCode = 0;
                EE.Message = "Spx_Collection.add():Errore nell'inserimento di (" + p_Key + ", " + p_Value + ") in " + p_Sezione + ". Vedi inner Exception";
                throw EE;
            }
        }
        #endregion
    }
}
