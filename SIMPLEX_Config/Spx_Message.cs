﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIMPLEX_Config
{
    /// <summary>
    /// 
    /// Valori predefiniti:
    /// 0. Errore generico  (vedere Eccezione d'origine).
    /// 1.Il parametro chiave contiene NULL
    /// 2.Sezione vuota.
    /// 3.Chiave non trovata.
    /// 4.Nome della sezione non valido o NULL
    /// 5.Nome della sezione già esistente.
    /// 6. Collezione senza sezioni (neanche quella di default)
    /// 7. La sezione specificata non esiste.
    /// 8. File di configurazione non valodo;
    /// 9. La chiave già esiste - Key already exists;
    /// 10. Nome del file non valido o non specificato.
    /// 11. Nome del file esistente.
    /// 12. Errore di scrittura nel file.
    /// </summary>
    public class Spx_Message : Exception
    {
        public int MessageCode;
        private String l_Msg;
        private Exception l_InnerException;

        //MASK
        new public String Message
        {
            get
            {
                return l_Msg;
            }
            set
            {
                l_Msg = value;
            }
        }

        new public Exception InnerException
        {
            get
            {
                return l_InnerException;
            }
            set
            {
                l_InnerException = value;
            }
        }
    }
}
